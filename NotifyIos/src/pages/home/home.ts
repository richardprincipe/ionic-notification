import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Cordova } from '@ionic-native/core';

declare var cordova;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  notify() {
    console.log(cordova.plugins.notification.local);
    cordova.plugins.notification.local.schedule([
      {
        id:1,
        title: 'First',
        text: 'Every minute',
        foreground: true
      },
      {
        id:2,
        title: 'Second',
        text: 'Every minute 2',
        foreground: true,
        trigger: { every: 'minute' }
      }]);

    /*this.localNotifications.schedule(
      {
        id: new Date().getTime(),
        title: 'Title notification',
        text: 'Daily notification',
        every: 'day'        
      }
    );*/
  }

}
